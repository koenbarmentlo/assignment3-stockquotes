/*jslint browser: true, plusplus:true*/
describe("showData", function () {
    beforeEach(function () {
        stockquote.init();
        stockquote.mode = "";
        stockquote.generateTestData();
    });

    it("Should have 99 tr elements", function () {
        expect(document.body.querySelectorAll("tr").length).toBe(99);
    });
});

describe("generateTestData", function () {
    beforeEach(function () {
        stockquote.init();
        stockquote.mode = "";
        stockquote.generateTestData();
    });

    it("stockquote.stockdata should have 99 items", function () {
        expect(stockquote.stockdata.length).toBe(99);
    });
});

describe("createXHRRequest", function () {
    it("XhrRequest should be instanceof XMLHttpRequest", function () {
        var request = stockquote.createXHRRequest("GET", "www.google.nl")
        expect(request).not.toBe(undefined);
        expect(request instanceof XMLHttpRequest).toBe(true);
    });
});

describe("handleResponse", function () {
    var json = '{"query": {"count": 27,"created": "2015-03-15T18:05:19+01:00","lang": "en-US","diagnostics": {"publiclyCallable": "true","url": {"execution-start-time": "1","execution-stop-time": "67","execution-time": "66","content": "http:\/\/download.finance.yahoo.com\/d\/quotes.csv?s=ibm+orcl+bcs+stt+jpm+lgen.l+ubs+db+ben+cs+bk+kn.pa+gs+lm+ms+mtu+ntrs+gle.pa+bac+av+sdr.l+dodgx+slf+sl.l+nmr+ing+bnp.pa&f=sl1d1t1c1ohgv&e\u200c?=.csv"},"user-time": "68","service-time": "66","build-version": "0.2.1867"},"results": {"row": [{"col0": "BCS","col1": "18.81","col2": "03\/15\/2015","col3": "06:05pm","col4": "1.39","col5": "N\/A","col6": "N\/A","col7": "N\/A","col8": "8900"},{"col0": "STT","col1": "62.03","col2": "03\/15\/2015","col3": "06:05pm","col4": "-4.67","col5": "N\/A","col6": "N\/A","col7": "N\/A","col8": "0"}]}}}';
    var event = {
        target: {
            responseText: json,
        }
    }
    it("HandleResponse should trigger dataloadedEvent", function () {
        var eventTriggered = false;
        stockquote.quoteContainer.addEventListener("dataloaded", function () {
            eventTriggered = true;
        });
        stockquote.handleResponse(event);
        expect(eventTriggered).toBe(true);
    });
    it("With this json string stockquote.stockdata.length should be 2.", function () {
        stockquote.handleResponse(event);
        expect(stockquote.stockdata.length).toBe(2);
    });
});
