/*globals stockquote, XDomainRequest, io */
/*jslint browser: true*/
(function () {
    "use strict";
    window.stockquote = {

        dataLoadedEvent: null,

        urlAjax: "http://server7.tezzt.nl/~theotheu/stockquotes/index.php",

        stockdata: null,

        quoteContainer: null,

        socket: io("http://server7.tezzt.nl:1333"),

        settings: {
            refreshTimeOut: 2000,
            mode: ""
        },

        init: function () {
            var titleH1Node = document.createElement("h1");
            titleH1Node.innerHTML = "Realtime Stockquotes";
            document.body.appendChild(titleH1Node);
            stockquote.quoteContainer = document.createElement("div");
            stockquote.quoteContainer.id = "container";
            stockquote.dataLoadedEvent = document.createEvent("Event");
            stockquote.dataLoadedEvent.initEvent("dataloaded", true, true);
            stockquote.quoteContainer.addEventListener("dataloaded", stockquote.showData);
            document.body.appendChild(stockquote.quoteContainer);
            stockquote.dataRetriever();
        },

        dataRetriever: function () {
            switch (stockquote.settings.mode) {
            case "ajax":
                return stockquote.retrieveDataAjax();
            case "ws":
                return stockquote.retrieveDataWS();
            default:
                return stockquote.generateTestData();
            }
        },

        createXHRRequest: function (method, url) {
            var request;
            request = new XMLHttpRequest();
            if (request.withCredentials !== undefined) {
                // Chrome/Firefox.
                request.open(method, url, true);
            } else if (typeof XDomainRequest) {
                // IE.
                request = new XDomainRequest();
                request.open(method, url);
            } else {
                // Not supported browsers.
                request = null;
            }
            return request;
        },

        //Websocket
        retrieveDataWS: function () {
            stockquote.socket.on('stockquotes', function (data) {
                stockquote.stockdata = data.query.results.row;
                stockquote.quoteContainer.dispatchEvent(stockquote.dataLoadedEvent);
            });
        },

        retrieveDataAjax: function () {
            var request;
            request = stockquote.createXHRRequest('GET', stockquote.urlAjax);
            request.addEventListener("load", stockquote.handleResponse);
            setTimeout(stockquote.dataRetriever, stockquote.settings.refreshTimeOut);
            request.send();
        },

        handleResponse: function (e) {
            var responseText = e.target.responseText;
            stockquote.stockdata = JSON.parse(responseText).query.results.row;
            stockquote.quoteContainer.dispatchEvent(stockquote.dataLoadedEvent);
        },

        generateTestData: function () {
            var jsonArr = [],
                i,
                startDateTime = new Date(2012, 0, 1),
                endDateTime = new Date();
            for (i = 1; i < 100; i += 1) {                
                jsonArr.push({
                    col0: "company: " + i,
                    col1: Math.floor(Math.random() * 1000) + 1,
                    col2: new Date(startDateTime.getTime() + Math.random() * (endDateTime.getTime() - startDateTime.getTime())).toDateString(),
                    col3: new Date(startDateTime.getTime() + Math.random() * (endDateTime.getTime() - startDateTime.getTime())).toLocaleTimeString(),
                    col4: Math.round((i - 50) * Math.floor(Math.random() * 2) % 10).toString(),
                    col5: (Math.floor(Math.random() * 1000) + 1).toString(),
                    col6: (Math.floor(Math.random() * 1000) + 1).toString(),
                    col7: (Math.floor(Math.random() * 1000) + 1).toString(),
                    col8: (Math.floor(Math.random() * 1000) + 1).toString()
                });
            }
            stockquote.stockdata = jsonArr;
            stockquote.quoteContainer.dispatchEvent(stockquote.dataLoadedEvent);
            setTimeout(stockquote.generateTestData, stockquote.settings.refreshTimeOut);
        },

        clearContainer: function () {
            var container,
                i;
            container = document.querySelector("#container");
            for (i = 0; i < container.childNodes.length; i += 1) {
                container.removeChild(container.childNodes[i]);
            }
        },

        showData: function () {
            var tableNode,
                rowCount,
                rowNode,
                cellNode,
                property;
            stockquote.clearContainer();
            tableNode = document.createElement("table");
            for (rowCount = 0; rowCount < stockquote.stockdata.length; rowCount += 1) {
                rowNode = document.createElement("tr");
                for (property in stockquote.stockdata[rowCount]) {
                    if (typeof stockquote.stockdata[rowCount][property] === "string") {
                        cellNode = document.createElement("td");
                        cellNode.innerHTML = stockquote.stockdata[rowCount][property];
                        if (property === "col4") {
                            if (stockquote.stockdata[rowCount][property][0] === "-") {
                                rowNode.className += " redbackground";
                            } else {
                                rowNode.className += " greenbackground";
                            }
                        }
                        rowNode.appendChild(cellNode);
                    }
                }
                tableNode.appendChild(rowNode);
            }
            stockquote.quoteContainer.appendChild(tableNode);
            return tableNode;
        }

    };
    stockquote.init();
}());