Document all concepts and your implementation decisions.


#Flow of the program
TODO: Improve this flow
1. init
1. retrieve data
1. draw data
1. go to step 2


#Concepts
For every concept the following items:
- short description
- code example
- reference to mandatory documentation
- reference to alternative documentation (authoritive and authentic)

I added server folder because the static analyzer won't work without it.

###Objects, including object creation and inheritance
TODO:
There are a few different ways to create objects in JavaScript.

var person1 = new Object();
person.name = "Koen";
person.age = 22;

var person2 = {
	name: “Nicholas”,
	age: 29,
	job: “Software Engineer”,
	sayName: function(){
		alert(this.name);
	}
};

Factory pattern
function createPerson(name, age, job){
	var o = new Object();
		o.name = name;
		o.age = age;
		o.job = job;
		o.sayName = function(){
		alert(this.name);
	};
	return o;
}
ver person3 = createPerson("Koen", 22, "Software Developer");

Constructor pattern
function Person(name, age, job){
	this.name = name;
	this.age = age;
	this.job = job;
	this.sayName = function(){
		alert(this.name);
	};
}
var person4 = new Person("Koen", 22, "Software Developer");

Prototype pattern
function Person(){
}
Person.prototype.name = "Koen";
Person.prototype.age = 20;
Person.prototype.job = "Software Developer";
Person.prototype.sayName = function(){
	alert(this.name);
};
var p1 = new Person();
p1.name = "Henkie";
p1.age = 89;

Alternative notation prototype pattern
function Person(){
}
Person.prototype = {
	name : "Koen",
	age : 22,
	job : “Software Developer”,
	sayName : function () {
		alert(this.name);
	}
};

Inheritance
function SuperType(){
	this.property = true;
}
SuperType.prototype.getSuperValue = function(){
	return this.property;
};
function SubType(){
	this.subproperty = false;
}
//inherit from SuperType
SubType.prototype = new SuperType();
SubType.prototype.getSubValue = function (){
	return this.subproperty;
};
var instance = new SubType();
alert(instance.getSuperValue()); //true

Professional JavaScript for Web Developers, Nicholas C. Zakas, Chapter 6: Object Oriented Programming.
Introduction to Object-Oriented JavaScript, https://developer.mozilla.org/en-US/docs/Web/JavaScript/Introduction_to_Object-Oriented_JavaScript?redirectlocale=en-US&redirectslug=JavaScript%2FIntroduction_to_Object-Oriented_JavaScript

###websockets
TODO:
WebSockets adds support for duplex-channel communication between client and server. The client can send requests to the server and the server can send data to the client without a request from the client.

retrieveDataWS: function () {
	stockquote.socket.on('stockquotes', function (data) {
		stockquote.stockdata = data.query.results.row;
		stockquote.quoteContainer.dispatchEvent(stockquote.dataLoadedEvent);
	});
},
Professional JavaScript for Web Developers, Nicholas C. Zakas, Chapter 21: Ajax and Comet.
JavaScript Programming, Jon Raasch, Part III Chapter 9: Going Real-Time With WebSockets.

###XMLHttpRequest
TODO:

XMLHttpRequest is a JavaScript object that is standardized by W3C. With this object you can retrieve data from the internet without a page refresh.

var request;
request = new XMLHttpRequest();
request.open('GET', url, true); //GET can also be POST if you want to do a POST request. url is a string variable with the URL.
request.addEventListener("load", stockquote.handleResponse);
handleResponse: function (e) {
	var responseText = e.target.responseText;
}

Professional JavaScript for Web Developers, Nicholas C. Zakas, Chapter 21: Ajax and Comet.
XMLHttpRequest: https://developer.mozilla.org/en-US/docs/Web/API/XMLHttpRequest?redirectlocale=en-US&redirectslug=DOM%2FXMLHttpRequest

###AJAX
TODO:
AJAX is a group of web technologies which are used to create asynchronous web applications. AJAX is used to send or retrieve data from an external source on the internet.

var request = new XMLHttpRequest();
request.open('get', 'send-ajax-data.php');
 
// Track the state changes of the request.
request.onreadystatechange = function () {
    var DONE = 4; // readyState 4 means the request is done.
    var OK = 200; // status 200 is a successful return.
    if (request.readyState === DONE) {
        if (request.status === OK) {
            alert(request.responseText); // 'This is the returned text.'
        } else {
            alert('Error: ' + request.status); // An error occurred during the request.
        }
    }
};
request.send(null);

Professional JavaScript for Web Developers, Nicholas C. Zakas, Chapter 21: Ajax and Comet.
AJAX: https://developer.mozilla.org/en-US/docs/Glossary/AJAX

###Callbacks
TODO:
A callback is a function that takes a function pointer as a parameter. 

function f(text, someFunction) {
	someFunction(text);
}
f("Hello world!", alert);

Callbacks: https://developer.mozilla.org/en-US/docs/Mozilla/js-ctypes/js-ctypes_reference/Callbacks
Advanced JavaScript: Callback Design Pattern and Callback Function: http://www.c-sharpcorner.com/UploadFile/dacca2/advance-javascript-callback-design-pattern-and-callback-fun/

###How to write testable code for unit-tests
TODO:
Write independent functions. Functions which do not depend on other functions:
retrieveDataWS: function () {
	stockquote.socket.on('stockquotes', function (data) {
		stockquote.stockdata = data.query.results.row;
		stockquote.quoteContainer.dispatchEvent(stockquote.dataLoadedEvent);
	});
},

Give functions return values with the result of the function. Or let the function fill or change a global variable:
With this code you can test if stockquote.stockdata = properly changed.
generateTestData: function () {
	var jsonArr = [],
		i;
	for (i = 1; i < 100; i += 1) {
		jsonArr.push({
			col0: "company: " + i,
			col1: Math.floor(Math.random() * 1000) + 1,
			col4: Math.round((i - 50) * Math.floor(Math.random() * 2) % 10).toString()
		});
	}
	stockquote.stockdata = jsonArr;
	stockquote.quoteContainer.dispatchEvent(stockquote.dataLoadedEvent);
	setTimeout(stockquote.generateTestData, stockquote.settings.refreshTimeOut);
},

Unit Testing: https://developer.mozilla.org/en-US/Add-ons/SDK/Tutorials/Unit_testing
Jasmine: https://jasmine.github.io/2.2/introduction.html
